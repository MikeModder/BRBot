package main

import (
	"time"
)

type config struct {
	Token string `json:"token"`
}

type afk struct {
	Set     time.Time
	Message string
}
