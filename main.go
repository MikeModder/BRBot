package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
)

var botCfg config
var brbList = make(map[string]afk)

func init() {
	log.Println("Loading and parsing config...")
	cfgFile, e := os.Open("config.json")
	if e != nil {
		log.Fatalf("[error] failed to open config.json: %s\n", e.Error())
	}
	cfgParser := json.NewDecoder(cfgFile)
	e = cfgParser.Decode(&botCfg)
	if e != nil {
		log.Fatalf("[error] failed to open config.json: %s\n", e.Error())
	}
}

func main() {
	// Discord stuff here
	dg, e := discordgo.New(fmt.Sprintf("Bot %s", botCfg.Token))
	if e != nil {
		fmt.Printf("[error] error getting new session: %s\n", e.Error())
		return
	}

	// Add handlers
	dg.AddHandler(newMessage)
	dg.AddHandler(discordReady)

	e = dg.Open()
	if e != nil {
		fmt.Printf("[error] error opening client: %s\n", e.Error())
		return
	}

	fmt.Printf("Logged in as %s, hit CTRL-C to stop\n", dg.State.User.Username)
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}

func discordReady(s *discordgo.Session, _ *discordgo.Ready) {
	s.UpdateStatus(0, "Managing AFK lists...")
}

func newMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.Bot {
		return
	}
	c := m.Content

	// is the author currently afk?
	if checkAFK(m.Author.ID) {
		// They are, remove them from the AFK list
		removeAFK(s, m)
	}

	// check if the message starts with either "brb" or "brb,"
	// if it does, strip the prefix and set "message" equal to the result
	if strings.HasPrefix(c, "brb") {
		addAFK(s, m, strings.TrimPrefix(c, "brb"))
	} else if strings.HasPrefix(c, "brb,") {
		addAFK(s, m, strings.TrimPrefix(c, "brb,"))
	} else {
		// It doesn't start with any keywords, so we need to loop over the mentions
		// and see if any of them are marked afk
		for i := 0; i < len(m.Mentions); i++ {
			u := m.Mentions[i]
			if checkAFK(u.ID) {
				// This mentioned user if AFK, tell them!
				s.ChannelMessageSend(m.ChannelID, fmt.Sprintf(":warning: ***%s*** is currently AFK!\n```%s```", u.String(), brbList[u.ID].Message))
			}
		}
	}
}

func addAFK(s *discordgo.Session, msg *discordgo.MessageCreate, afkMessage string) {
	if len(afkMessage) == 0 {
		afkMessage = "No message provided"
	} else if len(afkMessage) > 500 {
		s.ChannelMessageSend(msg.ChannelID, fmt.Sprintf(":x: ***%s***, your AFK message must be under 500 characters!", msg.Author.String()))
		return
	}

	brbList[msg.Author.ID] = afk{
		Set:     time.Now(),
		Message: afkMessage,
	}
	s.ChannelMessageSend(msg.ChannelID, fmt.Sprintf(":wave: See you later ***%s***!", msg.Author.String()))
}

func checkAFK(id string) bool {
	_, has := brbList[id]
	return has
}

func removeAFK(s *discordgo.Session, msg *discordgo.MessageCreate) {
	if !checkAFK(msg.Author.ID) {
		log.Printf("[wtf] removeAFK called for %s (%s) but not marked as afk\n", msg.Author.String(), msg.Author.ID)
		return
	}

	s.ChannelMessageSend(msg.ChannelID, fmt.Sprintf("Welcome back ***%s***!", msg.Author.String()))
	delete(brbList, msg.Author.ID)
}
